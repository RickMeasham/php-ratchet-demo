# INSTALL #

You'll need to use composer to set up the dependencies in the vendor directory. 

# RUNNING #

Each of the examples from /src/MyApp/App0.php through /src/MyApp/App4.php can be run using the associated script in /bin

    > php bin/server3.php

All these scripts will open a port on the local machine. The final example is invoked using `server4-ws.php` that will set up a websocket server that can be accessed opening the /www directory in a browser.

# CONTENTS #

## App 0 ##

This is just the framework required to set up a server. It does nothing whatsoever. You can connect to it and send messages, but it won't care.

## App 1 ##

We've now fleshed out the `onOpen`, `onClose` and `onError` methods. When you connect to the server, it will greet you and remember you exist. When you disconnect it will tidy up. 

## App 2 ##

This version implements a rudimentary `onMessage` method. Anything a client says will be distributed to all other clients.

## App 3 ##

Messages don't need to be text. They can be JSON. And that gives us the ability to send messages that are commands rather than strings to send to other clients. At this stage, we only implement the `message` command that distributes the provided message to all other clients.

## App 4 ##

The `nick` command is implemented which means users can identify as something other than the server connection resource ID.

This can be run in either local mode (`/bin/server4.php`) or as a webservice server (/bin/server4-ws.php`). When run as a WebService, open /www/index.htm in a browser.

# ACKNOWLEDGEMENTS #

* The [Ratchet Project](http://socketo.me/)
* Code in /src is based on the [Hello World example](http://socketo.me/docs/hello-world)
* Code in /www is adapted from SitePoint's [How to Quickly Build a Chat App with Ratchet](https://www.sitepoint.com/how-to-quickly-build-a-chat-app-with-ratchet/), but modified slightly to use JSON for messaging.

# LICENCE #

## WWW ##

Code in /www is released under the MIT licence [by Wern Ancheta the original author](https://github.com/anchetaWern/sitepoint_codes).

Copyright 2015 Wern Ancheta

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


## Original Code ##

Original code in this project is released under the MIT licence:

Copyright 2017 Rick Measham

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.