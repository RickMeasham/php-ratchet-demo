<?php
	require dirname(__DIR__) . '/vendor/autoload.php';

	use Ratchet\Server\IoServer;
	use MyApp\App3;

	$server = IoServer::factory(
		new App3(),
		8081
	);

	$server->run();