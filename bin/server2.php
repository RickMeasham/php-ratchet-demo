<?php
	require dirname(__DIR__) . '/vendor/autoload.php';

	use Ratchet\Server\IoServer;
	use MyApp\App2;

	$server = IoServer::factory(
		new App2(),
		8081
	);

	$server->run();