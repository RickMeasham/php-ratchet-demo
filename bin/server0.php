<?php
	require dirname(__DIR__) . '/vendor/autoload.php';

	use Ratchet\Server\IoServer;
	use MyApp\App0;

	$server = IoServer::factory(
		new App0(),
		8081
	);

	$server->run();