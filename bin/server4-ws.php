<?php
	require dirname(__DIR__) . '/vendor/autoload.php';

	use Ratchet\Server\IoServer;
	use Ratchet\Http\HttpServer;
	use Ratchet\WebSocket\WsServer;

	use MyApp\App4;

	$server = IoServer::factory(
		new HttpServer(
			new WsServer(
				new App4()
			)
		),
		8081
	);

	$server->run();