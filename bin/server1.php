<?php
	require dirname(__DIR__) . '/vendor/autoload.php';

	use Ratchet\Server\IoServer;
	use MyApp\App1;

	$server = IoServer::factory(
		new App1(),
		8081
	);

	$server->run();