<?php
	require dirname(__DIR__) . '/vendor/autoload.php';

	use Ratchet\Server\IoServer;
	use MyApp\App4;

	$server = IoServer::factory(
		new App4(),
		8081
	);

	$server->run();