<?php
	namespace MyApp;
	use Ratchet\MessageComponentInterface;
	use Ratchet\ConnectionInterface;

	class App1 implements MessageComponentInterface {
		protected $clients;

		public function __construct() {
			$this->clients = new \SplObjectStorage;
		}

		public function onOpen(ConnectionInterface $conn) {
			$this->clients->attach($conn);
			echo "New connection! ({$conn->resourceId})\n";

			$conn->send("Welcome. You are user {$conn->resourceId}\n");
		}

		public function onClose(ConnectionInterface $conn) {
			$this->clients->detach($conn);
			echo "Connection {$conn->resourceId} has disconnected\n";
		}

		public function onError(ConnectionInterface $conn, \Exception $e) {
			echo "An error has occurred: {$e->getMessage()}\n";
			$conn->close();
		}


		public function onMessage(ConnectionInterface $from, $msg) {}

	}