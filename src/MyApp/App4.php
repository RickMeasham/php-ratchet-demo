<?php
	namespace MyApp;

	// {"command":"message","message":"Hello world"}
	// {"command":"nick","nick":"Rick"}

	use Ratchet\MessageComponentInterface;
	use Ratchet\ConnectionInterface;

	class App4 extends App1 implements MessageComponentInterface {
		private $connectionNicknames = array();

		public function onMessage( ConnectionInterface $from, $msg ){

			$data = json_decode( $msg );

			$command = $data->command;
			echo "$command ({$from->resourceId}): $msg\n";

			switch ( $command ){
				case 'nick':
					$this->handleNick( $from, $data );
					break;
				case 'message':
					$this->handleMessage( $from, $data );
					break;
				default:
					$from->send(
						json_encode(
							array(
								'status'  => 'error',
								'message' => 'Malformed request',
								'from'    => 'system'
							)
						)
					);

			}
		}

		private function handleNick( $from, $data ){
			$oldNick = array_key_exists( $from->resourceId, $this->connectionNicknames )
				? $this->connectionNicknames[ $from->resourceId ]
				: $from->resourceId;

			$newNick = $data->nick;

			$this->connectionNicknames[ $from->resourceId ] = $newNick;

			foreach ( $this->clients as $client ) {
				$client->send(json_encode(array(
					'status' => 'ok',
					'message' => "$oldNick is now known as $newNick",
					'from' => 'system'
				)));
			}
		}

		private function handleMessage( $from, $data ){
			foreach ( $this->clients as $client ) {
				if ( $from !== $client ) {
					$client->send(json_encode(array(
						'status' => 'ok',
						'message' => $data->message,
						'from' => $this->connectionNicknames[ $from->resourceId ]
					)));
				}
			}
		}

	}