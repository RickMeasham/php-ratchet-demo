<?php
	namespace MyApp;

	// {"command":"message","message":"Hello world"}

	use Ratchet\MessageComponentInterface;
	use Ratchet\ConnectionInterface;

	class App3 extends App1 implements MessageComponentInterface {

		public function onMessage( ConnectionInterface $from, $msg ){

			$data = json_decode( $msg );

			$command = $data->command;
			echo "$command ({$from->resourceId}): $msg";

			switch ( $command ){
				case 'message':
					$this->handleMessage( $from, $data );
					break;
				default:
					$from->send(
						json_encode(
							array(
								'status'  => 'error',
								'message' => 'Malformed request',
								'from'    => 'system'
							)
						)
					);

			}
		}

		private function handleMessage( $from, $data ){
			foreach ( $this->clients as $client ) {
				if ( $from !== $client ) {
					$client->send(json_encode(array(
						'status' => 'ok',
						'message' => $data->message,
						'from' => $from->resourceId
					)));
				}
			}
		}

	}