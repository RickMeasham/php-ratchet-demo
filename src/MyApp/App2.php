<?php
	namespace MyApp;
	use Ratchet\MessageComponentInterface;
	use Ratchet\ConnectionInterface;

	class App2 extends App1 implements MessageComponentInterface {

		public function onMessage(ConnectionInterface $from, $msg) {

			foreach ($this->clients as $client) {
				if ($from !== $client) {
					$client->send($msg);
				}
			}

		}

	}